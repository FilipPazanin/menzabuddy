package com.example.menzabuddy;

import android.net.Uri;

public class UserInformation {


    public  String name;
    public String address;
    public String college;
    public String hobies;
    public String age;
    public int sex;
    public String pictureUrl;

    public String time;
    public String menza;

    public UserInformation(String name, String address, String college, String hobies, String age, int sex, String pictureUrl, String time, String menza) {
        this.name = name;
        this.address = address;
        this.college = college;
        this.hobies = hobies;
        this.age = age;
        this.sex = sex;
        this.pictureUrl = pictureUrl;
        this.time = time;
        this.menza = menza;
    }

    public UserInformation(String name, String address, String college, String hobies, String age, int sex, String pictureUrl) {
        this.name = name;
        this.address = address;
        this.college = college;
        this.hobies = hobies;
        this.age = age;
        this.sex = sex;
        this.pictureUrl = pictureUrl;

    }
    public String getHobies() {
        return hobies;
    }

    public String getAge() {
        return age;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getMenza() {
        return menza;
    }

    public void setMenza(String menza) {
        this.menza = menza;
    }

    public int getSex() {
        return sex;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setHobies(String hobies) {
        this.hobies = hobies;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCollege() {
        return college;
    }

    public void setCollege(String college) {
        this.college = college;
    }

    public UserInformation(){

    }

    public UserInformation(String name, String address, String college){
        this.name =name;
        this.address =address;
        this.college = college;
    }
}
