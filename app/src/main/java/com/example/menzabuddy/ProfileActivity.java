package com.example.menzabuddy;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.Manifest;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.UserProfileChangeRequest;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;


import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.android.gms.tasks.OnSuccessListener;

import android.support.design.widget.TextInputLayout;


import org.chat21.android.ui.ChatUI;
import org.chat21.android.ui.contacts.activites.ContactListActivity;

import java.util.Map;

public class ProfileActivity extends AppCompatActivity implements  View.OnClickListener {

    private FirebaseAuth firebaseAuth;

    private TextView textViewUserEmail;

    private Button buttonLogout;

    private DatabaseReference databaseReference;

    private TextInputLayout editTextName, editTextAddress, editTextHobies, editTextAge;
   private  AutoCompleteTextView editTextCollege;

    private Button buttonSave;
    private Button chatButton;
    private Button listButton;

    private RadioGroup radioSexGroup;
    private RadioButton radioSexButton;

    ImageView ImgUserPhoto;
    static int PReqCode = 1;
    static int REQUESCODE = 1;
    Uri pickedImgUri;
    String photoUrl;
    Toolbar toolbar;



    private FloatingActionButton fab;
    private String menza;
    private  String time;

    String[] fakulteti = new String[]{"Agronomski fakultet",
            "Arhitektonski fakultet",
           " Edukacijsko-rehabilitacijski fakultet",
            "Ekonomski fakultet",
            "Fakultet elektrotehnike i računarstva",
            "Fakultet kemijskog inženjerstva i tehnologije",
            "Fakultet organizacije i informatike",
            "Fakultet političkih znanosti",
            "Fakultet prometnih znanosti",
            "Fakultet strojarstva i brodogradnje",
            "Farmaceutsko-biokemijski fakultet",
            "Filozofski fakultet",
            "Fakultet filozofije i religijskih znanosti",
            "Geodetski fakultet",
            "Geotehnički fakultet",
            "Građevinski fakultet",
            "Grafički fakultet",
            "Katolički bogoslovni fakultet",
            "Kineziološki fakultet",
            "Medicinski fakultet",
            "Metalurški fakultet",
            "Pravni fakultet",
            "Prehrambeno-biotehnološki fakultet",
            "Prirodoslovno-matematički fakultet",
            "Rudarsko-geološko-naftni fakultet",
            "Stomatološki fakultet",
            "Šumarski fakultet",
            "Tekstilno–tehnološki fakultet",
            "Učiteljski fakultet",
            "Veterinarski fakultet"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        firebaseAuth = FirebaseAuth.getInstance();


         toolbar = findViewById(R.id.toolbar);
         toolbar.setTitleTextAppearance(this,R.style.ToolbarTheme);
         toolbar.setTitleTextColor(0xFFFFFFFF);
         toolbar.setTitle("Menza Buddy");

        if (firebaseAuth.getCurrentUser() == null) {
            finish();
            startActivity(new Intent(this, LoginActivity.class));
        }


        databaseReference = FirebaseDatabase.getInstance().getReference();


        editTextAddress = findViewById(R.id.editTextAddress);
        editTextName = findViewById(R.id.editTextName);
        editTextCollege = findViewById(R.id.editTextCollege);
        editTextHobies = findViewById(R.id.editTextHobies);
        editTextAge = findViewById(R.id.editTextAge);
        radioSexGroup = (RadioGroup) findViewById(R.id.radioSex);
        photoUrl = "";
        menza = "";
        time="";
        chatButton=(Button) findViewById(R.id.chatButton);
        buttonSave = (Button) findViewById(R.id.buttonSave);
        listButton=(Button) findViewById(R.id.listButton);
        fab = findViewById(R.id.fab);

        //editTextName.setFocusableInTouchMode(true);


        FirebaseUser user = firebaseAuth.getCurrentUser();

        textViewUserEmail = (TextView) findViewById(R.id.textViewUserEmail);

        textViewUserEmail.setText("Welcome " + ((FirebaseUser) user).getEmail());


        buttonLogout = (Button) findViewById(R.id.buttonLogout);

        listButton.setOnClickListener(this);
        buttonLogout.setOnClickListener(this);
        buttonSave.setOnClickListener(this);
        chatButton.setOnClickListener(this);
        fab.setOnClickListener(this);


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.select_dialog_singlechoice, fakulteti);
        editTextCollege.setThreshold(1);
        editTextCollege.setAdapter(adapter);

        getUserInfo();


        ImgUserPhoto = findViewById(R.id.regUserPhoto);

        ImgUserPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Build.VERSION.SDK_INT >= 22) {
                    checkAndRequestForPermission();
                } else {
                    openGallery();
                }

            }
        });


    }


    private void getUserInfo() {
        FirebaseUser user = firebaseAuth.getCurrentUser();
        databaseReference.child("users").child(user.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists() && dataSnapshot.getChildrenCount() > 0) {
                    Map<String, Object> map = (Map<String, Object>) dataSnapshot.getValue();

                    if (map.get("name") != null) {
                        String name = map.get("name").toString();
                        editTextName.getEditText().setText(name);
                    }

                    if (map.get("address") != null) {
                        String address = map.get("address").toString();
                        editTextAddress.getEditText().setText(address);
                    }

                    if (map.get("college") != null) {
                        String college = map.get("college").toString();
                        editTextCollege.setText(college);
                    }

                    if (map.get("hobies") != null) {
                        String hobies = map.get("hobies").toString();
                        editTextHobies.getEditText().setText(hobies);
                    }

                    if (map.get("age") != null) {
                        String age = map.get("age").toString();
                        editTextAge.getEditText().setText(age);
                    }

                    if (map.get("sex") != null) {
                        int sex = Integer.parseInt(map.get("sex").toString());
                        radioSexButton = (RadioButton) findViewById(sex);
                        if(radioSexButton!=null) {
                            radioSexButton.toggle();
                        }

                    }


                    if (map.get("pictureUrl") != null) {
                        photoUrl = map.get("pictureUrl").toString();
                        String profileImageUrl = map.get("pictureUrl").toString();
                        //ako je prazan imageUrl onda stavi default sliku
                        if (profileImageUrl.length() >1) {


                            Glide.with(getApplication()).load(profileImageUrl).dontAnimate()
                                    .fitCenter()
                                    .override(500, 500).into(ImgUserPhoto);
                        }
                    }


                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void saveUserInformation() {



        int ageInt = 0;
        String name = editTextName.getEditText().getText().toString().trim();
        String add = editTextAddress.getEditText().getText().toString().trim();
        String college = editTextCollege.getEditableText().toString().trim();
        String hobies = editTextHobies.getEditText().getText().toString().trim();
        String age = editTextAge.getEditText().getText().toString().trim();
        int selectedIdSex = radioSexGroup.getCheckedRadioButtonId();

        //check name notu empty
        if (name.matches("")){
            Toast.makeText(this, "You did not enter a name", Toast.LENGTH_SHORT).show();
            return;
        }
        //check age number
        if (age.length() > 0) {
            ageInt = Integer.parseInt(age);
        }
        if (ageInt > 100 || ageInt < 15){
            Toast.makeText(this,"Enter a valid age number", Toast.LENGTH_SHORT).show();
            return;
        }
        UserInformation userInfo = new UserInformation(name, add, college, hobies, age, selectedIdSex, photoUrl);

        FirebaseUser user = firebaseAuth.getCurrentUser();
        databaseReference.child("users").child(user.getUid()).setValue(userInfo);


        Toast.makeText(this, "Information Saving...", Toast.LENGTH_LONG).show();

    }


    private void openGallery() {
        //TODO: open gallery intent and wait for user to pick an image !

        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        startActivityForResult(galleryIntent, REQUESCODE);
    }


    private void checkAndRequestForPermission() {


        if (ContextCompat.checkSelfPermission(ProfileActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(ProfileActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {

                Toast.makeText(ProfileActivity.this, "Please accept for required permission", Toast.LENGTH_SHORT).show();

            } else {
                ActivityCompat.requestPermissions(ProfileActivity.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        PReqCode);
            }

        } else
            openGallery();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && requestCode == REQUESCODE && data != null) {

            // the user has successfully picked an image
            Toast.makeText(ProfileActivity.this, "Please wait for image upload", Toast.LENGTH_SHORT).show();
            // we need to save its reference to a Uri variable

            pickedImgUri = data.getData();

            StorageReference mStorage = FirebaseStorage.getInstance().getReference().child("users_photos");
            final StorageReference imageFilePath = mStorage.child(textViewUserEmail.getText().toString().substring(8));
            imageFilePath.putFile(pickedImgUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                    // image uploaded succesfully
                    // now we can get our image url
                    imageFilePath.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {

                            // uri contain user image url
                            photoUrl = uri.toString();
                            //System.out.println(photoUrl);

                        }
                    });


                }
            });


            ImgUserPhoto.setImageURI(pickedImgUri);

        }
    }

    @Override
    public void onClick(View view) {
        if (view == buttonLogout) {
            firebaseAuth.signOut();
            finish();
            startActivity(new Intent(this, LoginActivity.class));
        }

        if (view == buttonSave) {


            saveUserInformation();
            getUserInfo();

        }
        if(view== chatButton){
        //    Intent intent = new Intent(this, ContactListActivity.class);
          //  intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // start activity from context
            ChatUI.getInstance().setContext(this);
            ChatUI.getInstance().openConversationsListActivity();
            //startActivity(intent);


        }
        if(view==listButton){
            startActivity(new Intent(this, ListActivity.class));
        }

        if (view == fab) {
            openDialog();
        }
    }

    private void openDialog() {
        ExampleDialog exampleDialog = new ExampleDialog();
        exampleDialog.show(getSupportFragmentManager(), "example dialog");

    }
}



