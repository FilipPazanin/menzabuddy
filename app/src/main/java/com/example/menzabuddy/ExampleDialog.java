package com.example.menzabuddy;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Calendar;
import java.util.HashMap;

public class ExampleDialog extends AppCompatDialogFragment {
    //private AutoCompleteTextView editTextUsername;
    private Spinner dropdown;
    private EditText chooseTime;
    String[] menze = new String[]{ "SD Stjepan Radić", "SD Cvjetno naselje", "SD Lašćina", "Borongaj",
            "Ekonomija", "SC Savska", "Medicina", "Veterina", "Šumarstvo", "FSB", "FER", "ALU", "TTF", "Superfaks", "Odeon", "Vemag" };


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.layout_dialog, null);

        builder.setView(view)
                .setTitle("Choose place and time")
                .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                })
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        FirebaseDatabase database = FirebaseDatabase.getInstance();
                        final DatabaseReference  mDatabaseRef = database.getReference();

                        //String menza = editTextUsername.getEditableText().toString();
                        final String menza = dropdown.getSelectedItem().toString();
                        final String time = chooseTime.getEditableText().toString();


                        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
                        final FirebaseUser user = firebaseAuth.getCurrentUser();
                        mDatabaseRef.child("zahtjevi").child(user.getUid()).child("menza").setValue(menza);
                        mDatabaseRef.child("zahtjevi").child(user.getUid()).child("time").setValue(time);
                        DatabaseReference userinfo = mDatabaseRef.child("users").child(user.getUid());
                        userinfo.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                Advertisement ad = new Advertisement();
                                ad.time=time;
                                ad.menza=menza;
                                ad.user_info=dataSnapshot.getValue(UserInformation.class);
                                mDatabaseRef.child("zahtjevi").child(user.getUid()).setValue(ad);
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                // ...
                            }
                        });



                    }
                });


        //editTextPassword = view.findViewById(R.id.edit_password);

       //1 ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.select_dialog_singlechoice, menze);
       // editTextUsername = view.findViewById(R.id.edit_username);
         dropdown = view.findViewById(R.id.edit_username);
       //3 editTextUsername.setThreshold(1);
        //2editTextUsername.setAdapter(adapter);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, menze);
        dropdown.setAdapter(adapter);

        chooseTime = view.findViewById(R.id.etChooseTime);
        chooseTime.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_UP){
                    Calendar calendar = Calendar.getInstance();
                    int currentHour = calendar.get(Calendar.HOUR_OF_DAY);
                    int currentMinute = calendar.get(Calendar.MINUTE);

                    TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker timePicker, int hourOfDay, int minutes) {
                            chooseTime.setText(hourOfDay + ":" + minutes);

                        }
                    }, currentHour, currentMinute, true);
                    timePickerDialog.show();
                    return true;
                }
                return false;
            }
        });
       /* chooseTime.setOnClickListener(new View.OnClickListener() {
               @Override
            public void onClick(View v) {

                Calendar calendar = Calendar.getInstance();
                int currentHour = calendar.get(Calendar.HOUR_OF_DAY);
                int currentMinute = calendar.get(Calendar.MINUTE);

                TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hourOfDay, int minutes) {
                        chooseTime.setText(hourOfDay + ":" + minutes);

                    }
                }, currentHour, currentMinute, true);
                timePickerDialog.show();
            }

        })*/


        return builder.create();

    }
}
