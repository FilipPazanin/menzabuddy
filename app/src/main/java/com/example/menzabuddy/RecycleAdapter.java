package com.example.menzabuddy;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.net.URL;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class RecycleAdapter extends RecyclerView.Adapter<RecycleAdapter.UserViewHolder> {

    private List<Advertisement> bookList;
    public CircleImageView image;

    public RecycleAdapter(List<Advertisement> bookList) {
        this.bookList = bookList;
    }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.user_list_row, parent, false);

        return new UserViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(UserViewHolder holder, int position) {
        Advertisement info=bookList.get(position);

        String hame=" ";
        String hollage=" ";
        if(info.user_info!=null){
            hame=info.user_info.name +"     " +info.user_info.age;
            hollage=info.user_info.college + "     " + info.time;
        }

        holder.name.setText(hame);
        holder.college.setText(hollage);

    }

    @Override
    public int getItemCount() {
        return bookList.size();
    }

    public class UserViewHolder extends RecyclerView.ViewHolder {

        public TextView name;
        public TextView college;

        public UserViewHolder(View view) {
            super(view);
            image = (CircleImageView) view.findViewById(R.id.appIconIV);
            name = (TextView) view.findViewById(R.id.aNametxt);
            college=(TextView) view.findViewById(R.id.aVersiontxt);
        }
    }


}
