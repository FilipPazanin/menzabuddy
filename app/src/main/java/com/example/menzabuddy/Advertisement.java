package com.example.menzabuddy;

import java.util.Date;



public class Advertisement {

    public String time;
    public String menza;
    public UserInformation user_info;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getMenza() {
        return menza;
    }

    public void setMenza(String menza) {
        this.menza = menza;
    }

    public UserInformation getUser_info() {
        return user_info;
    }

    public void setUser_info(UserInformation user_info) {
        this.user_info = user_info;
    }


    @Override
    public String toString() {
        return time+" "+ menza + "   "+ user_info.name;
    }
}
