package com.example.menzabuddy;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.google.firebase.database.*;

import java.util.ArrayList;

import java.util.Comparator;
import java.util.List;


public class ListActivity extends AppCompatActivity {

    private List<Advertisement> bookList = new ArrayList<>();
    private RecyclerView recyclerView;
    private RecycleAdapter mAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);

        mAdapter = new RecycleAdapter(bookList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        initAdData();



    }
    private void  initAdData(){
        DatabaseReference databaseReference=FirebaseDatabase.getInstance().getReference().child("zahtjevi");
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot data:dataSnapshot.getChildren()){

                    Advertisement ad= data.getValue(Advertisement.class);
                    if(ad!=null){
                        Log.w("jedan", "uno");
                        Log.w("tag", ad.toString());
                    }

                    bookList.add(ad);
                }
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // ...
            }
        });
        Log.w("tag", " dolje");
        for(Advertisement ad: bookList){
            Log.w("tag", ad.toString());
            Log.w("tag", "new line");
        }
        Log.w("books size"," "+ bookList.size() );
          //  mAdapter.notifyDataSetChanged();

    }

    private class MenzaComparator implements Comparator<UserInformation> {

        @Override
        public int compare(UserInformation o1, UserInformation o2) {
            return o1.getMenza().compareTo(o2.getMenza());
        }
    }

    private class TimeComparator implements Comparator<UserInformation> {

        @Override
        public int compare(UserInformation o1, UserInformation o2) {
            String[] sati_min1 = o1.getTime().split(":");
            String[] sati_min2 = o2.getTime().split(":");


            return Integer.valueOf(sati_min1[0]).compareTo(Integer.valueOf(sati_min2[0])) != 0 ? Integer.valueOf(sati_min1[0]).compareTo(Integer.valueOf(sati_min2[0])) : Integer.valueOf(sati_min1[1]).compareTo(Integer.valueOf(sati_min2[1]));
        }
    }
}
