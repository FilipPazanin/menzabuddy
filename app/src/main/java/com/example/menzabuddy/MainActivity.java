package com.example.menzabuddy;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;

public class MainActivity extends AppCompatActivity {
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ImageView srkiView= findViewById(R.id.imageView3);
        srkiView.setVisibility(View.VISIBLE);
        Button yesButton= findViewById(R.id.button2);
        Button noButton= findViewById(R.id.button);
        yesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recordAnswer("bi");
                Toast toast=Toast.makeText(getApplicationContext(),"Imas ju care",Toast.LENGTH_SHORT);
                toast.show();

            }
        });
        noButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recordAnswer("nebi");
                Toast toast=Toast.makeText(getApplicationContext(),"Jbg bit ce bolljih",Toast.LENGTH_SHORT);
                toast.show();

            }
        });

    }

    private void recordAnswer(String odgovor){
        Bundle bundle = new Bundle();
        mFirebaseAnalytics= FirebaseAnalytics.getInstance(this);
        mFirebaseAnalytics.logEvent(odgovor,bundle);

    }
}
